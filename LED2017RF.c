//!make
#include "LED2017RF.h"
#include "ws2812b.h"
#include "Remote.h"
#include "iap.h"

int numStrips=1,numLed=15;//MAXLED;
unsigned ticks=0;
unsigned char level=64;
unsigned long prefix=0xE25EUL;
RGB rgb= {16,0,0};

void ReadCfg() {
	unsigned long id=0;
	int i,n=0;
	BYTE b;

	Delay(10);
	for (i=3;i>=0;i--) {
		b=IapReadByte(IAP_ADDRESS+i);
		id<<=8;
		id|=(unsigned long)b;
	}
	if (id!=0xFFFFFFFFUL&&id!=0UL) prefix=id;

	for (i=1;i>=0;i--) {
		b=IapReadByte(IAP_ADDRESS+4+i);
		n<<=8;
		n|=(unsigned long)b;
	}
	if (n!=-1&&n!=0) numLed=n;

	n=0;
	for (i=1;i>=0;i--) {
		b=IapReadByte(IAP_ADDRESS+8+i);
		n<<=8;
		n|=(unsigned long)b;
	}
	if (n!=-1&&n!=0) numStrips=n;
}

BOOL WriteCfg() {
	unsigned long id=prefix;
	int i,n=numLed;

	IapEraseSector(IAP_ADDRESS);
	Delay(10);
	for (i=0;i<4;i++) {
		if (!IapProgramByte(IAP_ADDRESS+i,id)) return FALSE;
		id>>=8;
	}
	for (i=0;i<2;i++) {
		if (!IapProgramByte(IAP_ADDRESS+4+i,n)) return FALSE;
		n>>=8;
	}
	n=numStrips;
	for (i=0;i<2;i++) {
		if (!IapProgramByte(IAP_ADDRESS+8+i,n)) return FALSE;
		n>>=8;
	}
	return TRUE;
}

void timer0_int (void) __interrupt 1 {
	ticks++;
}

void initTimer0() {
	AUXR=0x80;//T0 in 1T mode
	TMOD=0;//set T0 as 16 bit autoreload timer/counter
	TR0=1;
	ET0=1;
	EA=1;
}

void initTimer2() {
	//T2H=T2_MS>>8;
	//T2L=T2_MS;
	AUXR|=0x04;//T2 in 1T mode
	AUXR&=~0x8;//counter mode
	AUXR|=0x10;//T2 start run
	//IE2|=4;
	EA=1;
}

void DumpByte(BYTE b) {
	int i,j;
	
	P3&=~0x04;
	for (j=0; j<15000; j++) ;
	for (i=0; i<8; i++,b<<=1) {
		P3|=0x4;
		for (j=(b&0x80)?15000:5000; j>=0; j--) ;
		P3&=~0x04;
		for (j=0; j<5000; j++) ;
	}
	for (j=0; j<15000; j++) ;
}

void SetNumLed() {
	int i,j,t,t0=ticks;
	unsigned char lastCmd=SETTINGS;
	BOOL refresh=TRUE;
	
	cmd=0;
	while (TRUE) {
		if (refresh) {
			int k=144/numLed;
			for (j=0; j<numStrips; j++)
				for (i=0; i<numLed; i++)
					WS2812_SendRGBData(k*i,0,144-k*i);
			for (i=numStrips*numLed;i<MAXLED;i++)
				WS2812_SendRGBData(0,0,0);
			WS2812_Latch();
			refresh=FALSE;
		}
		
		HandleRadio();
		
		if (ticks>t0) t=ticks-t0;
		else t=65536UL-t0+ticks;
		
		if (t<25 && cmd!=0 && lastCmd==cmd) 
			cmd=0;
		
		if (cmd!=0) {
			switch (cmd) {
				case MODE_PLUS:
					if (++numLed>MAXLED) numLed=MAXLED;
					break;
				case MODE_MINUS:
					if (--numLed<1) numLed=1;
					break;
				case SPEED_PLUS:
					if (++numStrips>MAXSTRIPS) numStrips=MAXSTRIPS;
					break;
				case SPEED_MINUS:
					if (--numStrips<1) numStrips=1;
					break;
				case SETTINGS:
					if (t>100) {
						WriteCfg();
						return;
					}
					break;
			}
			refresh=TRUE;
			lastCmd=cmd;
			cmd=0;
			t0=ticks;
		}
	}
}

void main (void) {
	int i,j,nRepeat=0,r,q,tot1,tot2;
	unsigned t0=0,t,tStart;
	unsigned char mode=AUTO,subMode=0,lastCmd=0,t2=0;

	for (i=0; i<numLed; i++)
		WS2812_SendRGBData(0,0,0);
	WS2812_Latch();

	ReadCfg();
	
	initTimer0();
	initTimer2();
	while (TRUE) {
		if (ticks>t0) t=ticks-t0;
		else t=65536UL-t0+ticks;
		
		if (cmd!=0) {
			if (cmd==lastCmd) {
				if (++nRepeat>10) {
					nRepeat=0;
					if (cmd==LOCK) {
						lastCmd=cmd=0;
						Pairing();
					}
					else if (cmd==SETTINGS)  {
						lastCmd=cmd=0;
						SetNumLed();
					}
					t0=ticks;
				}
			}
			else  {
				lastCmd=cmd;
				nRepeat=0;
			}
		}
		switch (cmd) {
		case POWER:
		case RED:
		case GREEN:
		case BLUE:
		case YELLOW:
		case CYAN:
		case MAGENTA:
		case WHITE:
		case AUTO:
			mode=cmd;
			break;
		case BRIGHTNESS_PLUS:
			if (level<252) level+=4;
			break;
		case BRIGHTNESS_MINUS:
			if (level>=16) level-=4;
			break;
		case MODE_PLUS:
			if (mode!=AUTO || t<50) break;
			if (++subMode==4) subMode=0;
			t0=ticks;
			break;
		case MODE_MINUS:
			if (mode!=AUTO || t<50) break;
			if (--subMode==255) subMode=3;
			t0=ticks;
			break;
		}
		
		cmd=0;

		switch (mode) {
		case POWER:
			rgb.r=0;
			rgb.g=0;
			rgb.b=0;
			break;
		case RED:
			rgb.r=gamma8[level];
			rgb.g=0;
			rgb.b=0;
			break;
		case GREEN:
			rgb.r=0;
			rgb.g=gamma8[level];
			rgb.b=0;
			break;
		case BLUE:
			rgb.r=0;
			rgb.g=0;
			rgb.b=gamma8[level];
			break;
		case YELLOW:
			rgb.r=gamma8[level];
			rgb.g=gamma8[level];
			rgb.b=0;
			break;
		case CYAN:
			rgb.r=0;
			rgb.g=gamma8[level];
			rgb.b=gamma8[level];
			break;
		case MAGENTA:
			rgb.r=gamma8[level];
			rgb.g=0;
			rgb.b=gamma8[level];
			break;
		case WHITE:
			rgb.r=gamma8[level];
			rgb.g=gamma8[level];
			rgb.b=gamma8[level];
			break;
		case AUTO:
			t2=T2H;
			break;
		}

		
		q=level/numLed;	//quoziente
		r=level%numLed;	//resto

		for (j=0; j<numStrips; j++) {
			tot1=tot2=0;
			for (i=0; i<numLed; i++) {
				if (mode==AUTO) 
					switch (subMode) {
						case 3:
							rgb.g=0;
							if (t2<128&&(i&1)==0 || t2>128&&(i&1)==1) {
								rgb.r=gamma8[level];
								rgb.b=0;
							}
							else {
								rgb.r=0;
								rgb.b=gamma8[level];
							}
							break;
						case 1:
							rgb.r=0;
							if (t2<128&&(i&1)==0 || t2>128&&(i&1)==1) {
								rgb.g=gamma8[level];
								rgb.b=0;
							}
							else {
								rgb.g=0;
								rgb.b=gamma8[level];
							}
							break;
						case 2:
							rgb.b=0;
							if (t2<128&&(i&1)==0 || t2>128&&(i&1)==1) {
								rgb.g=gamma8[level];
								rgb.r=0;
							}
							else {
								rgb.g=0;
								rgb.r=gamma8[level];
							}
							break;
						case 0:
							if (j&1) {
								rgb.b=tot1;
								rgb.r=level-tot1;
							}
							else {
								rgb.r=tot1;
								rgb.b=level-tot1;
							}
							rgb.g=0;
							tot1+=q+1;
							if (tot2+=r>numLed) {
								tot1++;
								tot2-=numLed;
							}
							break;
					}

				WS2812_SendRGB(&rgb);
			}
		}
		tStart=ticks;
		while (tStart==ticks || tStart+1==ticks) HandleRadio();
	}
}


