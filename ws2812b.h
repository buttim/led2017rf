#ifndef __WS2812B_H__
#define __WS2812B_H__

typedef struct { unsigned char r,g,b;} RGB;

extern const unsigned char code gamma8[];

void WS2812_SendZero(void);
void WS2812_SendOne_(void);
void WS2812_SendByte_(unsigned char);
void WS2812_Latch(void);

void WS2812_SendRGB(RGB*);
void WS2812_SendRGBData(unsigned char,unsigned char,unsigned char);
#endif
