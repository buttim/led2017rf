//!make
#include "led2017rf.h"
#include "ws2812b.h"
#include "iap.h"
#include "Remote.h"

#define checkDuration(t,d) (/*(TF0==0) &&*/ (t)<((d)+(d)/4)*(FOSC/1E6) && (t)>((d)-(d)/4)*(FOSC/1E6))

int cmd;

static int status=0,bitPos=0,key=0;
static BOOL prefixOk=FALSE,pairing=FALSE;
static unsigned long newPrefix;

void Pairing() {
	int n=0,i;
	pairing=TRUE;
	bitPos=0;
	prefixOk=FALSE;
	
	for (i=0; i<numLed; i++)
		WS2812_SendRGBData(0,0,0);
	WS2812_Latch();
	WS2812_SendRGBData(0,0,64);
	WS2812_Latch();
	while (TRUE) {
		HandleRadio();
		if (cmd==LOCK) {
			if (newPrefix!=prefix && ++n==20) {
				pairing=FALSE;
				prefix=newPrefix;
				WriteCfg();
				WS2812_SendRGBData(0,255,0);
				WS2812_Latch();
				break;
			}
			cmd=0;
			newPrefix=0;
		}
	}
}

inline void HandleRadio() {
	unsigned short t;
	BYTE lastBit;
	
	if (cmd!=0) return;

	if (P35) {
		if (status==0) {
			status=1;

			TR0=0;
			t=(TH0<<8)+TL0;
			restartTimer0();
			if (!checkDuration(t,TBIT))
				prefixOk=bitPos=key=0;
		}
	}
	else {
		if (status==1) {//falling edge
			status=0;

			TR0=0;
			t=(TH0<<8)+TL0;
			TR0=1;
			if (checkDuration(t,T1H)) //bit 1
				lastBit=1;
			else if (checkDuration(t,T0H)) //bit 0
				lastBit=0;
			else {
				prefixOk=FALSE;
				bitPos=key=0;
				return;
			}
			if (prefixOk) {
				key<<=1;
				key|=lastBit;
				if (++bitPos==8) {
					cmd=key;
					prefixOk=FALSE;
					bitPos=key=0;
				}
			}
			else if (pairing) {
				newPrefix<<=1;
				newPrefix|=lastBit;
				if (++bitPos==17) {//prefix complete?
					bitPos=0;
					prefixOk=TRUE;
				}
			}
			else {
				BYTE b=0;
				
				if (bitPos==0) b=((BYTE*)&prefix)[2];//b=prefix>>16;
				else if (bitPos<8) b=((BYTE*)&prefix)[1]>>(8-bitPos);//((unsigned short)prefix)>>(16-bitPos);
				else b=((BYTE*)&prefix)[0]>>(16-bitPos);//((BYTE)prefix)>>(16-bitPos);
				b&=1;
				if (lastBit==b) {
					if (++bitPos==17) {//prefix complete?
						bitPos=0;
						prefixOk=TRUE;
					}
				}
				else {
					prefixOk=FALSE;
					bitPos=key=0;
				}
			}
		}
	}
}

