SDCCOPTS ?= --iram-size 128

SRC=LED2017RF.c iap.c Remote.c ws2812b.c
#SRC = $(wildcard *.c)
OBJ=$(patsubst %.c,build/%.rel, $(SRC))

build/%.rel: %.c
	mkdir -p $(dir $@)
	sdcc $(SDCCOPTS) -o build/ -c $<

all: led2017rf

led2017rf: $(OBJ)
	sdcc -o build/ $(SDCCOPTS) $^
	cp build/$@.ihx $@.hex

clean:
	rm -f *.ihx *.hex *.bin
	rm -f build/*
