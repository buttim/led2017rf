#ifndef __REMOTE_H__
#define __REMOTE_H__
#define POWER 0x02
#define SPEED_PLUS 0x0A
#define MODE_MINUS 0x0E
#define AUTO 0x10
#define MODE_PLUS 0x12
#define BRIGHTNESS_MINUS 0x14
#define SPEED_MINUS 0x16
#define BRIGHTNESS_PLUS 0x18
#define LOCK 0x1A
#define WHITE 0x1C
#define SETTINGS 0x1E
#define RED 0x20
#define GREEN 0x22
#define BLUE 0x24
#define YELLOW 0x26
#define CYAN 0x28
#define MAGENTA 0x2A

#define T1H 1240//uS pin high for 1 bit
#define T0H 480//uS pin high for 0 bit
#define TBIT 1640//uS bit duration
#define restartTimer0() { TR0=0; TH0=TL0=0; TR0=1; TF0=0; }

extern int cmd;
void Pairing();
inline void HandleRadio();
#endif
